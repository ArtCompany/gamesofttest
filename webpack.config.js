const path = require('path');
const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = {
    entry: './main.ts',
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 8080
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Development',
            template: 'index.html'
        })
    ],
    module: {
        rules: [
            { test: /\.css$/, loader: 'style!css' },
            {
                test: /\.scss$/,
                loaders: ['style-loader', 'raw-loader', 'sass-loader']
            },
            {
              enforce: 'pre',
              test: /\.js$/,
              loader: "source-map-loader"
            },
            {
              enforce: 'pre',
              test: /\.tsx?$/,
              use: "source-map-loader"
            },
            {
              test: /\.ts?$/,
              exclude: /node_modules/,
              use: [ 'babel-loader', 'ts-loader' ]
            },
            {
              test: /\.(png|jpg|json)$/, 
              use: "file-loader?name=[path][name].[ext]"
            }
          ],
        loaders: [
            { test: /\.json$/, loader: 'json-loader' },
            { test: /\.css$/, loader: 'style!css' },
            {
                test: /\.scss$/,
                loaders: ['style-loader', 'raw-loader', 'sass-loader']
            },
            {
                test: /\.js$/,
                include: [
                    path.resolve(__dirname, 'src')
                ],
                loader: 'babel-loader',
                query: {
                    presets: [
                        'babel-preset-es2015'
                    ].map(require.resolve),
                }
            }
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    externals: {
     pixi: "PIXI"
    },
   
    devtool: 'inline-source-map'
};
