const webpack = require('webpack');
const merge = require('webpack-merge');
const baseConfig = require('@skywind-group/sw-client-env/webpack/base.config');
const nodeEnv = process.env.NODE_ENV || 'development';
const platform = process.env.PLATFORM || 'desktop';
const isProduction = nodeEnv === 'production';
const nodeModules = `${__dirname}/node_modules`;
const localModules = `${__dirname}/lib`;

const libraries = [
    `${nodeModules}/gsap/src/uncompressed/TweenMax.js`,
    `${nodeModules}/pixi.js/dist/pixi.js`,
    `${nodeModules}/pixi-spine/bin/pixi-spine.js`,
    `${nodeModules}/pixi-particles/dist/pixi-particles.js`
];

const excludedClasses = [
    /ReelsCollapsing\w+/,
    /Collapsing\w+/,
    /Collapse\w+/,
    /Fall\w+/,
    /ReleaseSymbolsAction/,
    /WaitSpinResponseAction/
];

if (isProduction) {
    excludedClasses.push(
        /ReelsCollapsing\w+/,
        /Collapsing\w+/,
        /Collapse\w+/,
        /Fall\w+/,
        /ReleaseSymbolsAction/,
        /WaitSpinResponseAction/
    );
} else {
    libraries.unshift(`${localModules}/fpsmeter.js`);
}
baseConfig.entry[platform].unshift(...libraries);

module.exports = merge(baseConfig, {
    plugins: [
        new webpack.ProvidePlugin({
            FontFaceObserver: `${nodeModules}/fontfaceobserver/fontfaceobserver.standalone.js`,
            createjs: "createjs"
        })
    ],
    module: {
        rules: (() => excludedClasses.map(regexp => ({ test: regexp, use: 'null-loader' })))()
    }
});
