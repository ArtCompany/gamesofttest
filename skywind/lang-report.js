const fs = require('fs');
const path = require('path');
const webpackConfigName = './webpack.config.js';
const webpackConfig = require(webpackConfigName);
const gameDir = path.join(__dirname, 'data');
const wrapperDir = path.join(__dirname, 'node_modules', '@skywind-group', 'sw-wrapper', 'i18n');
const rowNamesGame = [];
const rowNamesWrapper = [];
const columns = [];

function getKeys(json, arr) {
    for (let key in json) {
        if (json.hasOwnProperty(key)) {
            arr.push(key);
        }
    }
}

function getValue(key, json) {
    const value = json[key];

    return (value || '--').replace('\n', '<br>');
}

let table = '';
webpackConfig.allLocales.forEach(locale => {
    const gameJson = JSON.parse(fs.readFileSync(path.join(gameDir, 'config', 'i18n', locale, 'i18n.json'), 'utf8'));
    const wrapperJson = JSON.parse(fs.readFileSync(path.join(wrapperDir, locale, 'i18n.json'), 'utf8'));
    const col = [];

    if (!rowNamesGame.length) {
        getKeys(gameJson, rowNamesGame);
        getKeys(wrapperJson, rowNamesWrapper);
    }

    rowNamesGame.forEach(key => col.push(getValue(key, gameJson)));
    rowNamesWrapper.forEach(key => col.push(getValue(key, wrapperJson)));
    columns.push(col);
    table += `<th>${locale}</th>`;
});

for (let i = 0; i < columns[0].length; i++) {
    let row = '';
    for (let j = 0; j < columns.length; j++) {
        row += `<td>${columns[j][i]}</td>`;
    }
    table += `<tr>${row}</tr>`;
}
const gameSettings = JSON.parse(fs.readFileSync(path.join(gameDir, 'settings.json'), 'utf8'));
const template = `<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">    
    <title>${gameSettings.ID}</title>
    <style>
        tr:nth-child(odd) {
            background-color: #ccc;
        }
    </style>
</head>
<body>
<h1>${gameSettings.NAME}</h1>
<table width="100%">${table}</table>
</body>
</html>`;


fs.writeFile(`${webpackConfig.output.path}/lang.html`, template, err => {
    if (err) {
        console.log(err);
    }
});
