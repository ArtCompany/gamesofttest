export const ReelConfig = {    
    reelStrip: [
        [0,2,1,1,1,0,2,2,0,2,1],
        [2,1,1,1,0,2,2,0,2,1,0],
        [1,0,2,1,1,1,0,2,2,0,2]
    ],
    initStripShift: 2,
    symbolWidth: 100,
    symbolHeight: 100,
    symbolOffset: 10,
    reelStartPosition: 300,
    reelNumber: 3,
    symbolNumber: 3,
    symbolAdditionalNumber: 2,
    reelWidth: 200,
};