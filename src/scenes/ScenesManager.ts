import { CoreRenderer } from '../core/CoreRenderer';
import { GlobalDispatcher } from '../core/GlobalDispatcher';
import { Scene } from './Scene';
import { Constants } from '../core/Constants';
import { MenuScene } from './MenuScene';
import { MainGameScene } from './mainGame/MainGameScene';
 
export class ScenesManager {
  private menuScene: Scene;
  private reelScene: Scene;
  
  constructor() {
    this.menuScene = this.createScene(Constants.MENU_SCENE, MenuScene);
    this.reelScene = this.createScene(Constants.MAIN_GAME_SCENE, MainGameScene);
    this.addListeners();   
  }

  private addListeners(): void {
    GlobalDispatcher.addEventListener(Constants.CHANGE_SCENE, (sceneId: string) => {
      this.goToScene(sceneId);
    });
    GlobalDispatcher.addEventListener(Constants.PAUSE_SCENE, (sceneId: string) => {
      if (CoreRenderer.currentScene) CoreRenderer.currentScene.pause();
    });    
  }

  private createScene(id: string, TScene: new () => Scene = Scene): Scene {
    if (CoreRenderer.scenes[id]) return undefined;
    var scene = new TScene();
    CoreRenderer.scenes[id] = scene;
    return scene;
  }
  
  private goToScene(id: string): boolean {
    if (CoreRenderer.scenes[id]) {
      if (CoreRenderer.currentScene) CoreRenderer.currentScene.pause();
        CoreRenderer.currentScene = CoreRenderer.scenes[id];
        CoreRenderer.rescale();
        CoreRenderer.currentScene.resume();
      return true;
    }
    return false;
  }
}



