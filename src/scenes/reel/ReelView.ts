import { ReelStrip } from "./ReelStrip";
import { Container } from "pixi.js";
import { GlobalDispatcher } from "../../core/GlobalDispatcher";
import { Constants } from "../../core/Constants";
import { ReelConfig } from "../../common/config";

export class ReelView extends Container {

    private reelStrips: ReelStrip[] = [];
    private spinCounter: number = 0;
    
    constructor() {
        super();                                    
    }

    public createReelStrips(): void {
        for (let index = 0; index < ReelConfig.reelNumber; index++) {
            const reelStrip: ReelStrip = new ReelStrip(index);
            reelStrip.x = ReelConfig.reelWidth * index;
            this.addChild(reelStrip); 
            this.reelStrips.push(reelStrip);          
        }
        this.createMask();
    }

    private disposeReelStrips(): void {

    }

    private createMask(): void {
        const graphics = new PIXI.Graphics();
        const maskWidth: number = ReelConfig.reelWidth * ReelConfig.reelNumber + 30;
        const maskHeight: number = (ReelConfig.symbolHeight + ReelConfig.symbolOffset) * 3; 
        const offset_y: number = (ReelConfig.symbolHeight + ReelConfig.symbolOffset) * 2; 
        graphics.beginFill(0xFFFF00);
        graphics.lineStyle(0, 0xFF0000);
        graphics.drawRect(-(ReelConfig.symbolHeight + ReelConfig.symbolOffset)  - 30, -5 + offset_y - ReelConfig.symbolHeight / 2, maskWidth, maskHeight);
        graphics.endFill();
        this.addChild(graphics);

        this.mask = graphics;
    }

    public start() {    
        this.spinCounter++;
        this.reelStrips.forEach((strip)=>{strip.startSpin();})    
    }

    public stop() {  
        if (this.spinCounter%4 === 0) {
            this.reelStrips[0].stopSpin(5);
            this.reelStrips[1].stopSpin(4);
            this.reelStrips[2].stopSpin(6);
            GlobalDispatcher.dispatch(Constants.START_WIN);
        } else {
            this.reelStrips.forEach((strip)=>{strip.stopSpin(3);})
        }             
    }
}
