import { Container } from "pixi.js";
import {ReelConfig} from "../../common/config";

export class Symbol extends Container {

    public readonly SQUARE: string = "SQUARE";
    public readonly ROUND: string = "ROUND";
    public readonly STAR: string = "STAR";

    constructor(protected _type: string, protected _index: number = 5) {
        super();
        this.createSymbol();                            
    }

    protected createSymbol(): void {
        let graphics = new PIXI.Graphics();

        switch (this._type) {
            case this.SQUARE:
                graphics.beginFill(0xFFFF00);
                graphics.lineStyle(1, 0xFF0000);
                graphics.drawRect(-ReelConfig.symbolWidth / 2, -ReelConfig.symbolHeight / 2, ReelConfig.symbolWidth - 2, ReelConfig.symbolWidth - 2);
                graphics.endFill();
                break;
            case this.ROUND:
                graphics.lineStyle(2, 0xFEEB77, 1);
                graphics.beginFill(0x650A5A, 1);
                graphics.drawCircle(0, 0, ReelConfig.symbolWidth / 2);
                graphics.endFill();
                break;
            case this.STAR:
                graphics.lineStyle(2, 0xFFFFFF);
                graphics.beginFill(0x35CC5A, 1);
                graphics.drawStar(0, 0, 5, ReelConfig.symbolWidth / 2, ReelConfig.symbolWidth / 4);
                graphics.endFill();
                break;
        
            default:
                break;
        }

        this.addChild(graphics);
    }

    public get type(): string {
        return this._type;
    }

    public get index():number {
        return this._index;
    }
}