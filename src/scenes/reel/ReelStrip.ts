import { Symbol } from "./Symbol";
import { Container } from "pixi.js";
import { TweenMax, Linear } from "gsap";
import { ReelConfig } from "../../common/config";
import { GlobalDispatcher } from "../../core/GlobalDispatcher";
import { Constants } from "../../core/Constants";

export class ReelStrip extends Container {

    public readonly SYMBOL_SPEED: number = 600;
    public readonly SYMBOL_STOP_SPEED: number = 1000;

    public readonly SPIN: string = "SPIN";
    public readonly STOP: string = "STOP";
    public readonly STOP_ENDED: string = "STOP_ENDED";
    public readonly WAIT: string = "WAIT";

    private endY: number;

    private stripState: string;

    private symbols: Symbol[] = [];
    private tweens: TweenMax[] = [];
    private currentSymbolFrame: number;
    private stopFrames: number[] = [];

    constructor(private reelId: number) {
        super();
        this.initStrip(ReelConfig.initStripShift);  
        this.stripState = this.WAIT;    
    }

    public startSpin(): void {
        this.symbols.forEach(
            (symbol) => {
                this.createTween(symbol, false);
            }
        )
        this.stripState = this.SPIN;
        this.stopFrames.length = 0;
    }

    public stopSpin(stopFrame: number): void {
        if (this.stripState === this.STOP) {
            return;
        }
        this.stripState = this.STOP;
        const strip: number[] = ReelConfig.reelStrip[this.reelId];
        for (let index = 0; index < ReelConfig.symbolNumber + ReelConfig.symbolAdditionalNumber; index++) {
            const stripIndex: number = stopFrame - ReelConfig.symbolNumber + ReelConfig.symbolAdditionalNumber < 0 ?
            this.stripState.length - 1 - (stopFrame - ReelConfig.symbolNumber + ReelConfig.symbolAdditionalNumber) :
            stopFrame - ReelConfig.symbolNumber + ReelConfig.symbolAdditionalNumber;
            this.stopFrames.push(stripIndex);            
        }       
        this.killTweens();
        this.addStopFrames();
        for (let index = 0; index < this.symbols.length; index++) {
            const symbol = this.symbols[index];
            this.createStopTween(symbol);
        }
    }

    private addSymbol(index: number, stripIndex: number, symbolIndex: number = 5): Symbol {
        const shift: number = ReelConfig.symbolHeight + ReelConfig.symbolOffset;
        const symbol: Symbol = new Symbol(this.getSymbolFromStrip(stripIndex), symbolIndex);
        symbol.y = index * shift;
        if (this.symbols.length > 0 && symbolIndex === ReelConfig.symbolNumber + ReelConfig.symbolAdditionalNumber) {
            symbol.y = this.getHighestSymbolY() - ReelConfig.symbolHeight - ReelConfig.symbolOffset;
        } else if (this.symbols.length > 0){
            //correct position, cause of different tick of tweens
            const corectedValue: number = this.getHighestSymbolY() % (ReelConfig.symbolHeight + ReelConfig.symbolOffset);
            symbol.y += corectedValue;
        }
        this.addChild(symbol);
        this.symbols.push(symbol);
        this.currentSymbolFrame = stripIndex;
        return symbol;
    }

    private removeSymbol(symbol: Symbol): void {
        const index: number = this.symbols.indexOf(symbol);
        this.symbols.splice(index, 1);
        this.removeChild(symbol);
        symbol = null;
    }

    private createTween(symbol: Symbol, stopMove: boolean): void {
        const speed: number = stopMove ? this.SYMBOL_STOP_SPEED : this.SYMBOL_SPEED
        const time: number = (this.endY - symbol.y) / speed;
                this.tweens.push(TweenMax.to(symbol, time, {
                    y: this.endY,  ease:Linear.easeNone,
                    onComplete:this.onSymbolTweenComplete.bind(this),
                    onCompleteParams:[symbol]}));
    }

    private createStopTween(symbol: Symbol): void {
        const endY: number = (ReelConfig.symbolAdditionalNumber + symbol.index) * (ReelConfig.symbolHeight + ReelConfig.symbolOffset);
        const time: number = (endY - symbol.y) / this.SYMBOL_STOP_SPEED;
                this.tweens.push(TweenMax.to(symbol, time, {
                    y: endY,  ease:Linear.easeNone,
                    onComplete:this.onSymbolStopTweenComplete.bind(this),
                    onCompleteParams:[symbol]}));
    }

    private onSymbolTweenComplete(oldSymbol: Symbol): void {
        const strip: number[] = ReelConfig.reelStrip[this.reelId];
        if (this.stripState === this.SPIN) {
            this.removeSymbol(oldSymbol);
            
            const stripIndex: number = this.currentSymbolFrame === strip.length - 1 ?
                                        0:
                                        this.currentSymbolFrame + 1;
            const newSymbol: Symbol = this.addSymbol(0, stripIndex); 
            this.createTween(newSymbol, false);
        } else if (this.stripState === this.STOP) {
            if (oldSymbol.index === ReelConfig.symbolNumber + ReelConfig.symbolAdditionalNumber) {
                this.removeSymbol(oldSymbol);
            }       
        }
    }

    private onSymbolStopTweenComplete(oldSymbol: Symbol): void {
        if (oldSymbol.index === ReelConfig.symbolNumber + ReelConfig.symbolAdditionalNumber) {
            this.removeSymbol(oldSymbol);
        } else {
            GlobalDispatcher.dispatch(Constants.REEL_STOPPED);
        }
        
    }

    private initStrip(stripShift: number): void {
        const stripLength: number = ReelConfig.symbolNumber + 2 * ReelConfig.symbolAdditionalNumber;
        const shift: number = ReelConfig.symbolHeight + ReelConfig.symbolOffset;
        this.endY = shift * stripLength;
        const strip: number[] = ReelConfig.reelStrip[this.reelId];
        
        for (let index = stripLength - 1; index >= 0; index--) {
            const stripIndex: number = 
                stripShift - ReelConfig.symbolAdditionalNumber < 0 ?
                stripLength - 1 - (stripShift - ReelConfig.symbolAdditionalNumber) :
                stripShift - ReelConfig.symbolAdditionalNumber
            this.addSymbol(index, stripIndex);
        }
    }

    private addStopFrames(): void {
        const stripLength: number = ReelConfig.symbolNumber + ReelConfig.symbolAdditionalNumber;
        const shift: number = ReelConfig.symbolHeight + ReelConfig.symbolOffset;
        this.endY = shift * stripLength;
        
        for (let index = 0; index < this.stopFrames.length; index++) {
            const stripIndex: number = this.stopFrames[index];
            this.addSymbol(-this.stopFrames.length + index, stripIndex, index - ReelConfig.symbolAdditionalNumber);
        }
    }

    private killTweens(): void {
        this.tweens.forEach(
            (tween)=>{
                tween.kill();
            })
        this.tweens.length = 0;
    }

    private getSymbolFromStrip(stripIndex: number): string {
        const strip: number[] = ReelConfig.reelStrip[this.reelId];
        const arr: string[] = ["SQUARE", "ROUND", "STAR"];
        return arr[strip[stripIndex]];
    }

    private getHighestSymbolY(): number {
        let result: number = Number.MAX_VALUE;
        this.symbols.forEach((symbol)=>{
            if(symbol.y < result) {
                result = symbol.y;
            }
        })
        return result;
    }
}