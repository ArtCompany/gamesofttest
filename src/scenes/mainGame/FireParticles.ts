import { Container } from "pixi.js";
import { Sprite } from "pixi.js";
/// <reference path="node_modules/pixi-particles/ambient.d.ts" />
const particles = require('pixi-particles');

export class FireParticles extends Container {
    constructor() {
        super();

        const emitterContainer: Sprite = new Sprite();
        this.addChild(emitterContainer);
        this.createFire(emitterContainer); 
    }

    private createFire(container: Sprite): void {
        // Create a new emitter
        // let emitter:PIXI.particles.Emitter = new PIXI.particles.Emitter(
        let emitter:any  = new particles.Emitter(

        // The PIXI.Container to put the emitter in
        // if using blend modes, it's important to put this
        // on top of a bitmap, and not use the root stage Container
        container,

        // The collection of particle images to use
        [PIXI.loader.resources["Fire"].texture, PIXI.loader.resources["Particle"].texture],


            // Emitter configuration, edit this to change the look
            // of the emitter
            {
                "alpha": {
                    "start": 0.45,
                    "end": 0
                },
                "scale": {
                    "start": 0.1,
                    "end": 0.5,
                    "minimumScaleMultiplier": 1
                },
                "color": {
                    "start": "#ebab4c",
                    "end": "#111314"
                },
                "speed": {
                    "start": 150,
                    "end": 300,
                    "minimumSpeedMultiplier": 1
                },
                "acceleration": {
                    "x": 0,
                    "y": 0
                },
                "maxSpeed": 0,
                "startRotation": {
                    "min": 250,
                    "max": 290
                },
                "noRotation": false,
                "rotationSpeed": {
                    "min": 0,
                    "max": 0
                },
                "lifetime": {
                    "min": 0.5,
                    "max": 1
                },
                "blendMode": "normal",
                "frequency": 0.001,
                "emitterLifetime": -1,
                "maxParticles": 500,
                "pos": {
                    "x": 0,
                    "y": 0
                },
                "addAtBack": true,
                "spawnType": "circle",
                "spawnCircle": {
                    "x": 0,
                    "y": 0,
                    "r": 20
                }
            }
        );

        // Calculate the current time
        var elapsed = Date.now();

        // Update function every frame
        var update = function(){

            // Update the next frame
            requestAnimationFrame(update);

            var now = Date.now();

            // The emitter requires the elapsed
            // number of seconds since the last update
            emitter.update((now - elapsed) * 0.001);
            elapsed = now;

            // Should re-render the PIXI Stage
            // renderer.render(stage);
        };

        // Start emitting
        emitter.emit = true;

        // Start the update
        update();
    }
}