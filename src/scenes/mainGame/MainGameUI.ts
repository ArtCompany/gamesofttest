import { Container } from "pixi.js";
import { SimpleButton } from "../../core/SimpleButton";
import { GlobalDispatcher } from "../../core/GlobalDispatcher";
import { Constants } from "../../core/Constants";

export class MainGameUI extends Container {

    private spinButton: SimpleButton;
    constructor() {
        super();
        this.spinButton = new SimpleButton();
        this.spinButton.x =730;
        this.spinButton.y =650;
        // this.spinButton.scale.x = this.spinButton.scale.y = 3;
        this.addChild(this.spinButton);

        this.addListeners();
    }

    private addListeners(): void {
        this.spinButton.on('button_click', ()=> {
            console.log('spin clicked');
            GlobalDispatcher.dispatch(Constants.SPIN_PRESSED);
        });
        GlobalDispatcher.addEventListener(Constants.ENABLE_SPIN_BUTTON, ()=>{
            this.spinButton.enable();
        });
        GlobalDispatcher.addEventListener(Constants.DISABLE_SPIN_BUTTON, ()=>{
            this.spinButton.disable();
        });
    }
}