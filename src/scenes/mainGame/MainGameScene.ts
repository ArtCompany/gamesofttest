import { Scene } from "../Scene";
import { ReelView } from "../reel/ReelView";
import { MainGameUI } from "./MainGameUI";
import { GlobalDispatcher } from "../../core/GlobalDispatcher";
import { Constants } from "../../core/Constants";
import { FireParticles } from "./FireParticles";

export class MainGameScene extends Scene {

    public readonly REEL_START_POS: number = 300;

    private reelView: ReelView;
    private reelUI: MainGameUI;
    private fireParticleLeft:FireParticles;
    private fireParticleRight:FireParticles;

    private state: string;/*
    private canStop: boolean = false;
    private canSpin: boolean = true;*/

    constructor() {
        super();
        this.state = 'wait';
        GlobalDispatcher.addEventListener(Constants.SPIN_PRESSED, () => {
            if(this.state === 'spin') {
                this.state = 'stop';
                this.reelView.stop();
                GlobalDispatcher.dispatch(Constants.DISABLE_SPIN_BUTTON);
            } else if (this.state === 'wait') {
                setTimeout(()=>{GlobalDispatcher.dispatch(Constants.ENABLE_SPIN_BUTTON);}, 1000);
                GlobalDispatcher.dispatch(Constants.DISABLE_SPIN_BUTTON);
                this.state = 'spin';
                this.reelView.start();
                this.hideWin();
            }
        });
        GlobalDispatcher.addEventListener(Constants.REEL_STOPPED, () => {
            if(this.state === 'stop') {
                this.state = 'wait'; 
                setTimeout(()=>{GlobalDispatcher.dispatch(Constants.ENABLE_SPIN_BUTTON);}, 200);
            } 
        });
        GlobalDispatcher.addEventListener(Constants.START_WIN, ()=>{
            this.fireParticleLeft.visible = true;
            this.fireParticleRight.visible = true;
        })
    }

    public resume() {
        if (!this.reelView) {
            this.reelView = new ReelView();
            this.reelView.x = this.REEL_START_POS;
            this.addChild(this.reelView);
            this.reelView.createReelStrips();

            this.fireParticleLeft = new FireParticles();
            this.fireParticleLeft.x = this.REEL_START_POS - 100;
            this.fireParticleLeft.y = 500;            
            this.addChild(this.fireParticleLeft);

            this.fireParticleRight = new FireParticles();
            this.fireParticleRight.x = this.REEL_START_POS + 550;
            this.fireParticleRight.y = 500;
            this.addChild(this.fireParticleRight);

            this.hideWin();
        }
        if(!this.reelUI) {
            this.reelUI = new MainGameUI();
            this.addChild(this.reelUI);
        }
        super.resume();
        
    }

    public pause() {        
        super.pause();
    }

    private hideWin(): void {
        this.fireParticleLeft.visible = false;
        this.fireParticleRight.visible = false;
    }
}