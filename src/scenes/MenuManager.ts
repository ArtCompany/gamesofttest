import { GlobalDispatcher } from "../core/GlobalDispatcher";
import { Constants } from "../core/Constants";

export class MenuManager {
    private test1Btn: HTMLElement = document.getElementById('Test1');
    private backMenu: HTMLElement = document.getElementById('BackMenu');

    public init(): void {
        this.hideGameButtons();
        this.showMenuButtons();
        this.addListeners();
    }

    private addListeners(): void {
        this.test1Btn.addEventListener('click', () => {    
            this.hideMenuButtons();
            this.showGameButtons();
            // GlobalDispatcher.dispatch(Constants.CHANGE_SCENE, Constants.CARD_SCENE);
            GlobalDispatcher.dispatch(Constants.CHANGE_SCENE, Constants.MAIN_GAME_SCENE);
        });
        
        this.backMenu.addEventListener('click', () => {    
            this.showMenuButtons();
            this.hideGameButtons();
            GlobalDispatcher.dispatch(Constants.CHANGE_SCENE, Constants.MENU_SCENE);
        });
    }

    private hideMenuButtons() {
        this.test1Btn.style.visibility = 'hidden';
    }

    private showMenuButtons() {
        this.test1Btn.style.visibility = 'visible';
    }

    private hideGameButtons() {
        this.backMenu.style.visibility = 'hidden';
    }

    private showGameButtons() {
        this.backMenu.style.visibility = 'visible';
    }
}