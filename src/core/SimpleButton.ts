import { Container, Sprite } from "pixi.js";

export class SimpleButton extends Container { 

    private isDown: boolean;
    private isOver: boolean;
    private isEnable: boolean = true;

    private button: Sprite;
    private textureButton: any;
    private textureButtonDown: any;
    private textureButtonOver: any;
    private textureButtonDIsable: any;
    
    constructor() {
        super();
        this.init();
    }

    public disable() {
        this.isEnable = false;
        this.button.interactive = false;
        this.button.buttonMode = false;
        this.button.texture = this.textureButtonDIsable;
    };

    public enable() {
        this.isEnable = true;
        this.button.interactive = true;
        this.button.buttonMode = true;
        this.button.texture = this.textureButton;
    };

    private init() {
         // create some textures from an image path
        this.textureButton = PIXI.loader.resources["SpinNormal"].texture;
        this.textureButtonDown = PIXI.loader.resources["SpinPress"].texture;
        this.textureButtonOver = PIXI.loader.resources["SpinOver"].texture;
        this.textureButtonDIsable = PIXI.loader.resources["SpinDisable"].texture;

        this.button = new PIXI.Sprite(this.textureButton);
        this.button.buttonMode = true;

        this.button.anchor.set(0.5);

        // make the button interactive...
        this.button.interactive = true;
        this.button.buttonMode = true;

        this.button
            // Mouse & touch events are normalized into
            // the pointer* events for handling different
            // button events.
            .on('pointerdown', this.onButtonDown.bind(this))
            .on('pointerup', this.onButtonUp.bind(this))
            .on('pointerupoutside', this.onButtonUp.bind(this))
            .on('pointerover', this.onButtonOver.bind(this))
            .on('pointerout', this.onButtonOut.bind(this));

            // Use mouse-only events
            // .on('mousedown', onButtonDown)
            // .on('mouseup', onButtonUp)
            // .on('mouseupoutside', onButtonUp)
            // .on('mouseover', onButtonOver)
            // .on('mouseout', onButtonOut)

            // Use touch-only events
            // .on('touchstart', onButtonDown)
            // .on('touchend', onButtonUp)
            // .on('touchendoutside', onButtonUp)

        // add it to the stage
        this.addChild(this.button);
    }

    private onButtonDown() {
        if (!this.isEnable) {return;}
        this.isDown = true;
        this.button.texture = this.textureButtonDown;
        this.alpha = 1;
    }

    private onButtonUp() {
        if (!this.isEnable) {return;}
        this.isDown = false;
        if (this.isOver) {
            this.button.texture = this.textureButtonOver;
        }
        else {
            this.button.texture = this.textureButton;
        }
        this.emit('button_click');
    }

    private onButtonOver() {
        if (!this.isEnable) {return;}
        this.isOver = true;
        if (this.isDown) {
            return;
        }
        this.button.texture = this.textureButtonOver;
    }

    private onButtonOut() {
        if (!this.isEnable) {return;}
        this.isOver = false;
        if (this.isDown) {
            return;
        }
        this.button.texture = this.textureButton;
    }

}