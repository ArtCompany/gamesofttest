import { Scene } from '../scenes/Scene';

export class CoreRenderer extends Scene {
  public static scenes: any = {};
  public static currentScene: Scene;
  public static renderer: any;
  public static ratio: number = 1;
  public static defaultWidth: number;
  public static defaultHeight: number;
  public static width: number;
  public static height: number;

    public static create(width: number, height: number, scale:Boolean=false) {
      if (document.readyState === 'complete' || document.readyState === 'interactive') {
        this.onDOMLoaded(width, height, scale);
      } else {
        document.addEventListener('DOMContentLoaded', ()=>{
            this.onDOMLoaded(width, height, scale);
        });
      }      
    }

    private static onDOMLoaded(width: number, height: number, scale:Boolean=false) {
      if (CoreRenderer.renderer) return this;
      this.defaultWidth = this.width = width;
      this.defaultHeight = this.height = height;
      CoreRenderer.ratio = width / height;
      CoreRenderer.renderer = PIXI.autoDetectRenderer(width, height);
      
      document.body.querySelector('.pixiContainer').appendChild(CoreRenderer.renderer.view);
        if (scale) {
          CoreRenderer.rescale();
          window.addEventListener('resize', CoreRenderer.rescale.bind(this), false);
        }                       
        this.enableFullScreen();
        requestAnimationFrame(this.loop.bind(this));
        return this;
    }

    public static rescale() {
        if (window.innerWidth / window.innerHeight >= CoreRenderer.ratio) {
          CoreRenderer.width = window.innerHeight * CoreRenderer.ratio;
          CoreRenderer.height = window.innerHeight;
        } else {
          CoreRenderer.width = window.innerWidth;
          CoreRenderer.height = window.innerWidth / CoreRenderer.ratio;
        }
      CoreRenderer.renderer.resize(CoreRenderer.width, CoreRenderer.height); 
      
      if (CoreRenderer.currentScene) {
        const ratio: number = CoreRenderer.width / this.defaultWidth;
        CoreRenderer.applyRatio(CoreRenderer.currentScene, ratio);
      }
      
    }

    public static applyRatio(displayObj: PIXI.DisplayObject, ratio: number) {
      if (ratio == 1) return;
      var object: any = displayObj;
      object.scale.x = ratio;
      object.scale.y = ratio;
    }
    

    public static enableFullScreen(){
      let isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||    // alternative standard method
          ((document as any).mozFullScreen || document.webkitIsFullScreen);

      let elem:any = document.documentElement as any;
      if (!isInFullScreen) {

        if (elem.requestFullscreen) {
          elem.requestFullscreen();
        } else if (elem.mozRequestFullScreen) { /* Firefox */
          elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
          elem.webkitRequestFullscreen();
        } else if (elem.msRequestFullscreen) { /* IE/Edge */
          elem.msRequestFullscreen();
        }
      }
  }

    public static loop() {
      requestAnimationFrame(function () { CoreRenderer.loop() });
      if (!CoreRenderer.currentScene || CoreRenderer.currentScene.isPaused()) return;
      CoreRenderer.currentScene.update(); 
      CoreRenderer.renderer.render(CoreRenderer.currentScene);
    }
}

