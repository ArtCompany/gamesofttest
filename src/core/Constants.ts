export const Constants = {
   CHANGE_SCENE: "CHANGE_SCENE",
   PAUSE_SCENE: "PAUSE_SCENE",
   CARD_SCENE: "CARD_SCENE",
   TEXT_SCENE: "TEXT_SCENE",
   FIRE_SCENE: "FIRE_SCENE",
   MAIN_GAME_SCENE: "MAIN_GAME_SCENE",
   MENU_SCENE: "MENU_SCENE",

   SPIN_PRESSED: "SPIN_PRESSED",
   REEL_STOPPED: "REEL_STOPPED",

   START_WIN: "START_WIN",

   ENABLE_SPIN_BUTTON: "ENABLE_SPIN_BUTTON",
   DISABLE_SPIN_BUTTON: "DISABLE_SPIN_BUTTON"
};

