import { CoreRenderer } from "../core/CoreRenderer";
import { ScenesManager } from "../scenes/ScenesManager";

export class Loader {
  constructor() {                       
    PIXI.loader.add('Fire', './assets/images/Fire.png')
                .add('Particle', './assets/images/particle.png')
                .add('SpinNormal', './assets/images/spin_bg_normal.png')
                .add('SpinOver', './assets/images/spin_bg_over.png')
                .add('SpinPress', './assets/images/spin_bg_press.png')
                .add('SpinDisable', './assets/images/spin_bg_disable.png')
               .on("complete", this.onComplete)
               .load(() => {
                 console.log("all loaded");
                });
    PIXI.loader.onError.add((err: any) => {console.error(err)});
  }

  private onComplete(){
    //safe area
    CoreRenderer.create(1000, 950, true);
    
    // CoreRenderer.create(window.innerWidth, window.innerHeight, true);
    new ScenesManager();
  }
}

