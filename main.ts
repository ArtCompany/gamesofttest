
import './assets/sass/main.scss';
import { MenuManager } from './src/scenes/MenuManager';
import { Loader } from './src/core/Loader';
const createFps = require('fps-indicator');
const fps = createFps({"color":"white"});

new MenuManager().init();
new Loader();
